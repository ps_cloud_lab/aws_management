## Section 1:
Links:
- https://www.py4e.com/
- http://diveintopython3.problemsolving.io/index.html
- https://docs.python.org/3.7/library/datetime.html
- https://docs.python.org/3.7/library/sys.html
- https://docs.python.org/3/howto/index.html
- https://docs.python.org/3/howto/argparse.html
- http://pymotw.com/3/logging/
- http://pymotw.com/3/random/
- https://pymotw.com/3/math/
- https://realpython.com/python-requests/
- https://realpython.com/python-logging/
- https://nedbatchelder.com/text/unipain.html
