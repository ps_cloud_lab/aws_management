#!/usr/bin/bash
Counter=0
Total=0
finished=false
while ! $finished;
do
	Counter=`expr $Counter + 1`
	echo "Please enter a number. Your count is: $Counter"
	read number
	Total=`expr $Total + $number`
	if [ $Counter -eq 10 ]; then
		echo $Total; 
		finished=true
	fi
done
